<div class="container">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
      <?php $s=1; ?>
      <div class="carousel-inner">
      @if (count($data['carousels']) > 0)
          @foreach ($data['carousels'] as $item)
               <div class="carousel-item {{ ($s==1) ? 'active':''}}">
                  <a href="#">
                      <img src="https://scholarship-positions.com/wp-content/uploads/2015/01/agbu-scholarship_home_2.png" class="d-block w-100" alt="...">
                    <div class="carousel-caption carousel-text" >
                      <h5 
                      class="carousel-text-h5"
                      >
                      Өөрт таалагдсан тэтгэлэгээ ол
                      </h5>
                    </div>
                  </a>
                </div> 
                {{$s = $s + 1}}
          @endforeach
      </div>
      @include('inc.search')
      @endif
    </div>
  </div>