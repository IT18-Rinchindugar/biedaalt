<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
  <div class="container">
      <a class="navbar-brand" href="http://localhost/BieDaalt/public/dashboard/">
          {{ config('app.name', 'Laravel') }}
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="http://localhost/BieDaalt/public/scholarship/">{{ __('Тэтгэлэгүүд') }}</a>
            </li>
          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="navbar-nav ml-auto">
              <!-- Authentication Links -->
              @guest
                  @if (Route::has('login'))
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('login') }}">{{ __('Нэвтрэх') }}</a>
                      </li>
                  @endif
                  
                  @if (Route::has('register'))
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('register') }}">{{ __('Бүртгэл үүсгэх') }}</a>
                      </li>
                  @endif
              @else
              <li class="nav-item">
                  {!!Form::open(['action' => 'ScholarshipController@search', 'method' => 'GET', 'class' => 'form-inline mr-2 my-lg-0'])!!}
                    {{Form::text('query', '', ['class' => 'form-control mr-2', 'placeholder' => 'Тэтгэлэг хайх '])}}
                  {{Form::submit('Хайх', ['class'=> 'btn btn-outline-success my-2 my-sm-0'])}}
                  {!!Form::close() !!}
              </li>
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                          {{ Auth::user()->name }}
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="http://localhost/BieDaalt/public/scholarship/create">Тэтгэлэг нэмэх
                     </a>
                     <a class="dropdown-item" href="http://localhost/BieDaalt/public/countries/create">Улс нэмэх
                     </a>
                          <a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                              @csrf
                          </form>
                      </div>
                  </li>
              @endguest
          </ul>
      </div>
  </div>
</nav>