<div class="sh_search">
  <div class="search_container">
      {!!Form::open(['action' => 'PagesController@search', 'method' => 'GET', 'class' => 'form-inline mr-2 my-lg-0', 'style' => 'width: 100%; margin-left: 10%;'])!!}
      <input class="form-control mr-sm-2" style="width: 60%; left: 0px;" name="search" placeholder="Та аль улсад суралцмаар байна вэ?" aria-label="Search">
      <button class="btn btn-primary my-2 my-sm-0" style="width: 30%" type="submit"><i class="fa fa-search" style="padding-right: 10px"></i>Тэтгэлэг хайх</button>
      {!!Form::close() !!}
  </div>
</div>