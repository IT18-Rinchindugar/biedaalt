@extends('layouts.client')
@section('content')
  @include('inc.landing')
  <div class="container">
    <br>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="http://localhost/BieDaalt/public/">Нүүр</a></li>
        <li class="breadcrumb-item active" aria-current="page">Хайлт үр дүн</li>
      </ol>
    </nav>
  <h1>Таны хайлтын үр дүнд {{count($scholarships)}} тэтгэлэг олдлоо</h1>
    <br>
    <div class="row">
      <div class="col-md-3">
        <div style="background-color: white; padding: 20px;">
          {!!Form::open(['action' => 'PagesController@search', 'method' => 'GET'])!!}
        @if (count($countries) > 0)
        <div class="form-group">
          <label for="">Тэтгэлэгт улс</label>
          <select name="country" class="form-control">
          @foreach ($countries as $country)
          <option value={{$country->name}}>{{$country->name}}</option>
          @endforeach
          </select>
        </div>
        @endif
        <div class="form-group">
          <label for="">Эрдэмийн зэрэг</label>
          <select name="degree" class="form-control">
            <option value="Бакалавр">Бакалавр</option>
            <option value="Магистр">Магистр</option>
            <option value="Доктор">Доктор</option>
          </select>
        </div>
        {{Form::submit('Хайлт хийх', ['class' => 'btn btn-primary btn-block'])}}
        {!! Form::close() !!}
        </div>
      </div>
      <div class="col-md-9">
        @if (count($scholarships) > 0)
          @foreach ($scholarships as $scholarship)
          <div class="col-md-12" style="margin-bottom: 50px;">
            <div style="background-color: white; padding: 20px;">
              <h3><a href="http://localhost/BieDaalt/public/scholarships/{{$scholarship->id}}">{{$scholarship->title}}</a></h3>
              <div style="margin-bottom: 15px">
                  <small> Нийтэлсэн огноо {{$scholarship->created_at}}</small>
              </div>
              <p>{!! $scholarship->description !!}</p>
              <div style="margin-left: 20px">
                  <div>
                      <h6><i class="fa fa-bell" style="padding-right: 10px"></i> {{$scholarship->degree}}</h6>
                      <h6><i class="fa fa-money" style="padding-right: 10px"></i> $1000 - ${{$scholarship->financing}}</h6>
                      <h6><i class="fa fa-calendar" style="padding-right: 10px"></i>{{$scholarship->created_at->format('Y.m.d')}} - {{$scholarship->endDate}}</h6>
                  </div>
              </div>
              <br>
              <a href="" class="btn btn-primary btn-md">Веб сайтаар зочлох</a>
              </div>
            </div>
          @endforeach
          @endif
    </div>       
  </div>
  </div>

@endsection
