@extends('layouts.client')
@section('content')
  @include('inc.landing')
  @if(count($scholarships_order) > 0)
  <div class="container">
    <br>
    <h1>Сүүлд зарлагдсан тэтгэлэгүүд</h1>
    <br>
    <div class="row">
          @foreach ($scholarships_order as $scholarship)
          <div class="col-md-4" style="margin-bottom: 50px;">
            <div class="card" style="width: 100%">
              <img class="card-img-top" style="height: 150px;" src="http://localhost/BieDaalt/public/storage/cover_images/{{$scholarship->cover_image}}" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title" style="height: 40px; overflow: hidden;">{{$scholarship->title}}</h5>
                <p class="card-text">
                  <div style="margin-left: 20px;">
                    <h6><i class="fa fa-flag" style="padding-right: 10px"></i> {{$scholarship->country}}</h6>
                    <h6><i class="fa fa-bell" style="padding-right: 10px"></i> {{$scholarship->degree}}</h6>
                    <h6><i class="fa fa-money" style="padding-right: 10px"></i> $1000 - ${{$scholarship->financing}}</h6>
                    <h6><i class="fa fa-calendar" style="padding-right: 10px"></i>{{$scholarship->created_at->format('Y.m.d')}} - {{$scholarship->endDate}}</h6>
                </div>
                </p>
                <a href="http://localhost/BieDaalt/public/scholarships/{{$scholarship->id}}" class="btn btn-primary btn-block">Дэлгэрэнгүй</a>
              </div>
            </div>
          </div>
          @endforeach
    </div>
    <br>
    <h1>Хамгийн ирэлттэй улс орнууд</h1>
    <br>
    <div class="row">
      @if(count($countries) > 0)
          @foreach ($countries as $country)
          <div class="col-md-4" style="margin-bottom: 50px;">
          <a href="http://localhost/BieDaalt/public/scholarships/search?country={{$country->name}}" class="card" style="width: 100%; text-decoration:none;">
              <img class="card-img-top" style="height: 250px;" src="http://localhost/BieDaalt/public/storage/cover_images/noimage.jpg" alt="Card image cap">
              <div class="card-body" style="display: flex; justify-content: center;">
                <h5 class="card-title" style="height: 20px; overflow: hidden;">{{$country->name}}</h5>
              </div>
            </a>
          </div>
          @endforeach
      @endif
    </div>
    
  </div>
  @endif
@endsection