@extends('layouts.client')
@section('content')
  @include('inc.landing')
  <div class="container">
    <br>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="http://localhost/BieDaalt/public/">Нүүр</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{$scholarship->title}}</li>
      </ol>
    </nav>
    <br>
    <div class="row">
      <div class="col-md-12">
          <h1>{{$scholarship->title}}</h1>
          <div style="margin: 20px;">
            <h6><i class="fa fa-flag" style="padding-right: 10px"></i> {{$scholarship->country}}</h6>
            <h6><i class="fa fa-bell" style="padding-right: 10px"></i> {{$scholarship->degree}}</h6>
            <h6><i class="fa fa-money" style="padding-right: 10px"></i> $1000 - ${{$scholarship->financing}}</h6>
            <h6><i class="fa fa-calendar" style="padding-right: 10px"></i>{{$scholarship->created_at->format('Y.m.d')}} - {{$scholarship->endDate}}</h6>
        </div>
        <a href="http://localhost/BieDaalt/public/scholarships/{{$scholarship->id}}" class="btn btn-primary">Веб сайтаар зочилох</a>
        <br>
        <p style="margin: 20px 0;">{!! $scholarship->description !!}</p>
        <img class="card-img-top" style="height: 35%;" src="http://localhost/BieDaalt/public/storage/cover_images/{{$scholarship->cover_image}}" alt="Card image cap">
        <div style="margin: 20px 0px;">
          <h2>Шаардлага</h2>
          <p style="margin: 20px 0;">{!! $scholarship->requirement !!}</p>
        </div>
        <div style="margin: 20px 0px;">
          <h2>Анкет</h2>
          <p style="margin: 20px 0;">{!! $scholarship->anket !!}</p>
        </div>
      </div>
    </div>
  </div>
@endsection