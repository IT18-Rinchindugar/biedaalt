@extends('layouts.app')
@section('content')
<a href="http://localhost/BieDaalt/public/scholarship" class="btn btn-primary btn-sm">Буцах</a>
<br>
<br>
<h1>Хайлтийн үр дүн</h1>
@if (count($scholarships) > 0)
@foreach ($scholarships as $scholarship)
    <div class="alert alert-light">
    <div class="row">
        {{-- <div class="col-md-4 col-sm-4">
        <img style="width: 100%" src="http://localhost/Demo/public/storage/cover_images/{{$post->cover_image}}" alt="">
        </div> --}}
        <div class="col-md-8 col-sm-8">
        <h3><a href="http://localhost/BieDaalt/public/scholarship/{{$scholarship->id}}">{{$scholarship->title}}</a></h3>
        <div style="margin-bottom: 15px">
            <small> Нийтэлсэн огноо {{$scholarship->created_at}}</small>
        </div>
        <p>{{$scholarship->description}}</p>
        <div style="margin-left: 20px">
            <div>
                <h6><i class="fa fa-bell" style="padding-right: 10px"></i> {{$scholarship->degree}}</h6>
                <h6><i class="fa fa-money" style="padding-right: 10px"></i> $1000 - ${{$scholarship->financing}}</h6>
                <h6><i class="fa fa-calendar" style="padding-right: 10px"></i>{{$scholarship->created_at->format('Y.m.d')}} - {{$scholarship->endDate}}</h6>
            </div>
        </div>
        <br>
        <a href="" class="btn btn-primary btn-md">Веб сайтаар зочлох</a>
        </div>
    </div>
    </div>
@endforeach
{{$scholarships->links("pagination::bootstrap-4")}}
@else
<p>Мэдээ байхгүй</p>
@endif
@endsection