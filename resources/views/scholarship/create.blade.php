@extends('layouts.app')
@section('content')
   <div class="rol">
       <div class="col"></div>
       <div class="col">
         <a href="http://localhost/BieDaalt/public/dashboard" class="btn btn-primary btn-sm">Буцах</a>
         <br>
         <br>
        <h1>Тэтгэлэг мэдээлэл</h1>
        {!! Form::open(['action' => 'ScholarshipController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group"> 
            <label for="text" class="col-form-label">{{ __('Гарчиг') }}</label>
                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}">
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
          </div>
            <div class="form-group"> 
                <label for="text" class="col-form-label">{{ __('Хаана') }}</label>
                    <input id="where" type="text" class="form-control @error('where') is-invalid @enderror" name="where" value="{{ old('where') }}">
                    @error('where')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
              </div>
              <div class="form-group">
                <label for="">Тэтгэлэгт улс</label>
                <select name="country" class="form-control">
                @foreach ($countries as $country)
                <option value={{$country->name}}>{{$country->name}}</option>
                @endforeach
                </select>
              </div>
            <div class="form-group"> 
              {{Form::label('degree','Эрдэмийн зэрэг')}}
              {{Form::select('degree', ['Бакалавр' => 'Бакалавр', 'Магистр' => 'Магистр', 'Доктор'=>'Доктор'], 'Бакалавр', ['class' => 'form-control'])}}
            </div>
            <div class="form-group"> 
                <label for="text" class="col-form-label">{{ __('Дуусах хугацаа') }}</label>
                <input id="endDate" type="date" class="form-control datetimepicker @error('endDate') is-invalid @enderror" name="endDate" value="{{ old('endDate') }}">
                @error('endDate')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
              </div>
            <div class="form-group"> 
              <label for="text" class="col-form-label">{{ __('Санхүүжилт') }}</label>
                  <input id="financing" type="number" class="form-control @error('financing') is-invalid @enderror" name="financing" value="{{ old('financing') }}">
                  @error('financing')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
            </div>
            <div class="form-group"> 
                <label for="text" class="col-form-label">{{ __('Тайлбар') }}</label>
                  <textarea id="description" rows="6" class="ckeditor form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}"></textarea>
                  @error('description')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
              <div class="form-group"> 
                <label for="text" class="col-form-label">{{ __('Шаардлага') }}</label>
                  <textarea id="requirement" rows="6" class="ckeditor form-control @error('requirement') is-invalid @enderror" name="requirement" value="{{ old('requirement') }}"></textarea>
                  @error('requirement')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
              </div>
              <div class="form-group"> 
                <label for="text" class="col-form-label">{{ __('Анкет') }}</label>
                <textarea id="anket" rows="6" class="ckeditor form-control @error('anket') is-invalid @enderror" name="anket" value="{{ old('anket') }}"></textarea>
                @error('anket')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
            <div class="form-group">
                <label for="text" class="col-form-label">{{ __('Зураг') }}</label>
                <br>
                <input id="cover_image" type="file" class="@error('cover_image') is-invalid @enderror" name="cover_image" value="{{ old('cover_image') }}">
                @error('cover_image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div> 
            {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
        {!! Form::close() !!}
       </div>
       <div class="col"></div>
   </div>
@endsection