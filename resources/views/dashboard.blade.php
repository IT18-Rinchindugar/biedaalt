@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Админ удирдах хэсэг') }}</div>
                <div class="card-body">
                <a href="http://localhost/BieDaalt/public/scholarship/create" class="btn btn-primary btn-sm">Тэтгэлэг нэмэх</a>
                <br>
                <table class="table mt-3">
                    <tr>
                        <th>Гарчиг</th>
                        <th></th>
                    </tr>
                    @if (count($scholarships) > 0)
                        @foreach ($scholarships as $scholarship)
                        <tr>
                            <th style="width: 70%;">{{$scholarship->title}}</th>
                            <th style="width: 30%;">
                                <a href="http://localhost/BieDaalt/public/scholarship/{{$scholarship->id}}/edit" class="btn btn-light">Edit</a>
                                {!!Form::open(['action' => ['ScholarshipController@destroy', $scholarship->id], 'method' => 'POST', 'class' => 'float-right'])!!}
                                {{Form::hidden('_method', 'DELETE')}}
                                {{Form::submit('Delete', ['class'=> 'btn btn-danger'])}}
                                {!!Form::close() !!}
                            </th>
                        </tr>
                        @endforeach
                    @endif
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
