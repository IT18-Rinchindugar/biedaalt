@extends('layouts.app')
@section('content')
   <div class="row">
       <div class="col"></div>
       <div class="col-8">
         <a href="http://localhost/BieDaalt/public/dashboard" class="btn btn-primary btn-sm">Буцах</a>
         <br>
         <br>
        <h1>Улс нэмэх</h1>
        {!! Form::open(['action' => 'CountriesController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group"> 
            <label for="text" class="col-form-label">{{ __('Улсын нэр') }}</label>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}">
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
          </div>
            {{Form::submit('Хадгалах', ['class' => 'btn btn-primary'])}}
        {!! Form::close() !!}
       </div>
       <div class="col"></div>
   </div>
@endsection