<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

Route::get('/', 'PagesController@index');
Route::get('scholarships/search', 'PagesController@search');
Route::get('scholarships/include', 'PagesController@search');
Route::get('/scholarships/{scholarship}','PagesController@show');

// Scholarship 
Route::get('scholarship/search', 'ScholarshipController@search');
Route::resource('scholarship', 'ScholarshipController');

// Countries 
Route::resource('countries', 'CountriesController');
// Auth
Auth::routes();

// Dashboard
Route::get('/dashboard', 'DashboardController@index');
