<?php

namespace App\Http\Controllers;

use App\Models\Scholarship;
use App\Models\Countries;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
        $data['carousels'] = Scholarship::orderBy('views','desc')->take(3)->get();
        $scholarships_order = Scholarship::orderBy('created_at','desc')->take(6)->get();
        $countries = Countries::all();
        $scholarships = [];
        return view('pages.home.index', 
            ['data'=>$data, 'scholarships' => $scholarships, 'scholarships_order' => $scholarships_order, 'countries' => $countries]);
    }

    public function search(Request $request) {
        $scholarships = Scholarship::orderBy('created_at','desc');
        if ($request->has('country')) {
            $scholarships = $scholarships->where('country', '=', $request->input('country'));
        }
        if ($request->has('degree')) {
            $scholarships = $scholarships->where('degree', '=', $request->input('degree'));
        }
        if ($request->has('search')) {
            $scholarships = $scholarships
            ->where('title', 'like', '%' . $request->input('search') . '%');   
        }
        $data['carousels'] = Scholarship::orderBy('views','desc')->take(3)->get();
        $countries = Countries::all();
        return view('pages.home.search', ['data'=>$data,'scholarships' => $scholarships->get(), 'countries' => $countries]);
    }
    public function show($id) {
        $data['carousels'] = Scholarship::orderBy('views','desc')->take(3)->get();
        $scholarships = [];
        $scholarship = Scholarship::find($id);
        return view('pages.home.show', ['data'=>$data, 'scholarships' => $scholarships, 'scholarship' => $scholarship]);
    }
}
