<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Scholarship;
use Illuminate\Support\Facades\Storage;
use App\Models\Countries;

class ScholarshipController extends Controller
{
       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['']]);
    }
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scholarships = Scholarship::orderBy('created_at','desc')->paginate(10);
        return view('scholarship.index')->with('scholarships', $scholarships);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Countries::orderBy('name','asc')->get();
        return view('scholarship.create')->with('countries', $countries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'where' => 'required',
            'endDate' => 'required',
            'financing' => 'required',
            'description' => 'required',
            'requirement' => 'required',
            'anket' => 'required',
            'cover_image' => 'image|nullable|max:20999',
            'country' => 'required',
        ]);

        // Handle File Upload
        if($request->hasFile('cover_image')){
            // Get filename with the extension
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);

        }else{
            $fileNameToStore = 'noimage.jpg';
        }

        // Create Scholarship 
        $scholarship = new Scholarship;
        $scholarship -> title = $request->input('title');
        $scholarship -> where = $request->input('where');
        $scholarship -> endDate = $request->input('endDate');
        $scholarship -> financing = $request->input('financing');
        $scholarship -> description = $request->input('description');
        $scholarship -> requirement = $request->input('requirement');
        $scholarship -> anket = $request->input('anket');
        $scholarship -> user_id = auth()->user()->id;
        $scholarship -> cover_image = $fileNameToStore;
        $scholarship -> country = $request->input('country');
        $scholarship -> save();
        return redirect('/dashboard')->with('success', 'Тэтгэлэгийг амжилттай нэмэгдлээ.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $scholarship = Scholarship::find($id);
        $scholarship->views = $scholarship->views + 1;
        $scholarship->save();
        return view('scholarship.show')->with('scholarship', $scholarship);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $scholarship = Scholarship::find($id);
        // Админ id эрхийг шалгах
        if(auth()->user()->id !== $scholarship->user_id) {
            return redirect('/dashboard')->with('error', 'Unauthorized Page');
        }
        return view('scholarship.edit')->with('scholarship', $scholarship);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'where' => 'required',
            'endDate' => 'required',
            'financing' => 'required',
            'description' => 'required',
            'requirement' => 'required',
            'anket' => 'required',
            'cover_image' => 'image|nullable|max:20999'
        ]);

        // Handle File Upload
        if($request->hasFile('cover_image')){
            // Get filename with the extension
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);

        }else{
            $fileNameToStore = 'noimage.jpg';
        }
        $scholarship = Scholarship::find($id);
        $scholarship -> title = $request->input('title');
        $scholarship -> where = $request->input('where');
        $scholarship -> endDate = $request->input('endDate');
        $scholarship -> financing = $request->input('financing');
        $scholarship -> description = $request->input('description');
        $scholarship -> requirement = $request->input('requirement');
        $scholarship -> anket = $request->input('anket');
        $scholarship -> cover_image = $fileNameToStore;
        $scholarship -> save();
        return redirect('/dashboard')->with('success', 'Тэтгэлэг шинчлэгдлээ.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $scholarship = Scholarship::find($id);
        if(auth()->user()->id !== $scholarship->user_id) {
            return redirect('/dashboard')->with('error', 'Unauthorized Page');
        }
        if($scholarship->cover_image != 'noimage.jpg') {
            // Image
            Storage::delete('public/cover_images/'.$post->cover_image);
        }
        $scholarship -> delete();
        return redirect('/dashboard')->with('success', 'Нийтлэл устлаа');
    }
    public function search(Request $request){
        $scholarships = Scholarship::orderBy('created_at','desc')->where('title', 'like', '%' . $request->input('query') . '%')
                ->paginate(6);
                
        return view('scholarship.search')->with('scholarships', $scholarships);
    }
}
