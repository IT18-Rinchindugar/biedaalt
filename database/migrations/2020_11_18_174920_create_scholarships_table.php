<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScholarshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scholarships', function (Blueprint $table) {
            $table-> increments('id');
            $table-> string('title');
            $table-> string('where');
            $table-> enum('degree', ['Бакалавр', 'Магистр','Доктор']);
            $table-> timestamp('endDate');
            $table-> double('financing');
            $table-> longText('description');
            $table-> mediumText('requirement');
            $table-> mediumText('anket');
            $table-> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scholarships');
    }
}
